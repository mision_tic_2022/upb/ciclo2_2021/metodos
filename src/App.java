import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {
        //int resultado = sumar(10, 2);
        //System.out.println("La sumatoria es: "+resultado);
        solicitar_datos();
    }

    /**
     * Método para sumar dos números,
     * recibe como parámetro dos enteros
     */
    public static int sumar(int numero_1, int numero_2){
        int suma = numero_1 + numero_2;
        return suma;
    }

    /**
     * Método para solicitar datos
     */
    public static void solicitar_datos(){

        try (Scanner entrada = new Scanner(System.in)){
            //Solicitar el primer número
            System.out.println("Por favor ingrese un número: ");
            int numero_1 = entrada.nextInt();
            //Solicitar el segundo número
            System.out.println("Por favor ingrese otro número");
            int numero_2  = entrada.nextInt();
            //Llamar el método sumar
            System.out.println("La suma es: "+sumar(numero_1, numero_2));
        } catch (Exception e) {
            //TODO: handle exception
            System.err.println("Error en la entrada de datos");
            System.out.println("Debe de digitar números enteros");
        }

    }
}
